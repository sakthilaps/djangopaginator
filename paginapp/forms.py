from django.forms import ModelForm

from.models import Employee

class Employeeform(ModelForm):

    class Meta:
        model = Employee
        fields = ['emp_name', 'emp_email', 'emp_adder', 'emp_salary']