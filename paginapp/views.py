from django.shortcuts import render
from.models import Employee
from.forms import Employeeform
from django.core.paginator import Paginator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.

def funpagin(request):
    print('Request method is:', request.method)

    if request.method=='GET':
        form = Employeeform()
    else:
        print(request.POST)
        form = Employeeform(data=request.POST)    

        if form.is_valid():
            employee = form.save()

            if employee is not None:
                employee.save()

                form = Employeeform()

    employees = Employee.objects.all()
    data = {'form':form, 'employees':employees}
    
    return render(request, "home.html", context=data)

def funindex(request):
    employees = Employee.objects.all()
    page = request.GET.get('page', 1)

    paginator = Paginator(employees, 5)
    try:
        users = paginator.page(page)
        
    except PageNotAnInteger:
        users = paginator.page(1)
    
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
        print(users)

    return render(request, "index.html", { 'users': users })
