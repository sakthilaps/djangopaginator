from django.db import models

# Create your models here.

class Employee(models.Model):
    emp_id = models.AutoField(primary_key=True)
    emp_name = models.CharField(max_length=50)
    emp_email = models.CharField(max_length=50)
    emp_adder = models.CharField(max_length=100)
    emp_salary = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)


    class Meta:
        db_table='Employee_tb'
                                
