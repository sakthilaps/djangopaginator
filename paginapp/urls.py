from django.urls import path
from .import views

urlpatterns = [
    path("", views.funpagin, name='home'),
    path("index/", views.funindex, name='index'),
]